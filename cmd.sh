# php -S 0.0.0.0:8011 -t public

docker rm $(docker stop $(docker ps -a -q --filter name="api_cron" --format="{{.ID}}"))
docker build -t "api_cron" .
docker run -d -i -p 8010:80 --name="api_cron" -v $PWD:/app "api_cron"
echo docker exec -it $(docker ps -a -q --filter name="api_cron" --format="{{.ID}}") /bin/bash
