<?php
namespace App\Http\Controllers\API\v1;

use Illuminate\Support\Facades\Http;
use GuzzleHttp\Promise\Utils;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;
use Illuminate\Http\Request;
use Datatables;

class CronController extends Controller
{
    public static function sync_host(Request $request){
        $arr_uri;
        $client = new \GuzzleHttp\Client();
        $results = \DB::select("SELECT * FROM olt_host");
        foreach ($results as $row) {
          $arr_uri[] = $client->requestAsync('GET', env('CRON_HOST')."/api/check-host/".$row->Code."?token=p2lbgWkFrykA4QyUmpHihzmc5BNzIABq")->then(
                        function ($response) {
                            $data = json_decode($response->getBody()->getContents());
                            if($data->response){
                              return $data->response;
                            }
                        }, function ($exception) {
                            return $exception->getMessage();
                        }
                      );
        }
        $promises = $arr_uri;
        $results = \GuzzleHttp\Promise\settle($promises)->wait();

        return makeResponse(200, 'success', "TIME IS: ".date('Y-m-d H:i:s'), $results);
    }

    public static function sync_config_host(Request $request){
        $arr_uri;
        $client = new \GuzzleHttp\Client();
        $results = \DB::select("SELECT * FROM olt_host WHERE ActiveStatus = 1");
        foreach ($results as $row) {
          $arr_uri[] = $client->requestAsync('GET', env('CRON_HOST')."/api/config-host/".$row->Code."?token=p2lbgWkFrykA4QyUmpHihzmc5BNzIABq")->then(
                        function ($response) {
                            $data = json_decode($response->getBody()->getContents());
                            if($data->response){
                              return $data->response;
                            }
                        }, function ($exception) {
                            return $exception->getMessage();
                        }
                      );
        }
        $promises = $arr_uri;
        $results = \GuzzleHttp\Promise\settle($promises)->wait();

        return makeResponse(200, 'success', "TIME IS: ".date('Y-m-d H:i:s'), $results);
    }

    public static function sync_ont(Request $request){
        $arr_uri;
        $client = new \GuzzleHttp\Client();
        $results = \DB::select("SELECT * FROM olt_host WHERE ActiveStatus = 1");
        foreach ($results as $row) {
          $arr_uri[] = $client->requestAsync('GET', env('CRON_HOST')."/api/ont-idle/".$row->Code."?token=p2lbgWkFrykA4QyUmpHihzmc5BNzIABq")->then(
                        function ($response) {
                            $data = json_decode($response->getBody()->getContents());
                            if($data->response){
                              return $data->response;
                            }
                        }, function ($exception) {
                            return $exception->getMessage();
                        }
                      );
        }
        $promises = $arr_uri;
        $results = \GuzzleHttp\Promise\settle($promises)->wait();

        return makeResponse(200, 'success', "TIME IS: ".date('Y-m-d H:i:s'), $results);
    }
}
