FROM alpine:edge

WORKDIR /var/www/app
COPY . /var/www/app
USER root

RUN apk add --update py-pip
RUN apk add --no-cache zip unzip curl sqlite nginx supervisor
RUN apk --update add wget \
    openssh-server tzdata curl git php7 php7-curl php7-openssl php7-iconv php7-json php7-mbstring php7-phar php7-dom --repository http://nl.alpinelinux.org/alpine/edge/testing/ && rm /var/cache/apk/*

RUN apk add bash
RUN sed -i 's/bin\/ash/bin\/bash/g' /etc/passwd

RUN apk add --no-cache php \
    php-common \
    php-fpm \
    php-pdo \
    php-opcache \
    php-zip \
    php-phar \
    php-iconv \
    php-cli \
    php-curl \
    php-openssl \
    php-mbstring \
    php-tokenizer \
    php-fileinfo \
    php-json \
    php-xml \
    php-xmlwriter \
    php-simplexml \
    php-dom \
    php-pdo_mysql \
    php-pdo_sqlite \
    php-tokenizer \
    php7-pecl-redis \
    php7-gd \
    php7-curl


RUN mkdir -p /etc/supervisor.d/
COPY /deploy/supervisor/supervisord.ini /etc/supervisor.d/supervisord.ini

RUN mkdir -p /run/php/
RUN touch /run/php/php7.4-fpm.pid
RUN touch /run/php/php7.4-fpm.sock

COPY /deploy/config/php-fpm/php-fpm.conf /etc/php7/php-fpm.conf
COPY /deploy/config/php-fpm/www.conf /etc/php7/php-fpm.d/www.conf
COPY /deploy/config/php7/php.ini /etc/php7/php.ini

RUN echo "daemon off;" >> /etc/nginx/nginx.conf
COPY /deploy/config/nginx/default.conf /etc/nginx/http.d/default.conf
COPY /deploy/config/nginx/fastcgi-php.conf /etc/nginx/fastcgi-php.conf
COPY /deploy/config/nginx/nginx.conf /etc/nginx/nginx.conf

RUN mkdir -p /run/nginx/
RUN touch /run/nginx/nginx.pid

RUN ln -sf /dev/stdout /var/log/nginx/access.log
RUN ln -sf /dev/stderr /var/log/nginx/error.log

ENV TZ=Asia/Jakarta
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone
RUN chmod -R 777 storage bootstrap

RUN mkdir -p /var/log/cron/
RUN mkdir -p /var/log/supervisor/
RUN mkdir -p /var/log/supervisor-error/

COPY /deploy/cron/root /var/spool/cron/crontabs/root

EXPOSE 80
CMD ["supervisord", "-c", "/etc/supervisor.d/supervisord.ini"]
