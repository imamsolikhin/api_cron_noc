<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\API\v1\CronController;
/*
  |--------------------------------------------------------------------------
  | Application Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register all of the routes for an application.
  | It is a breeze. Simply tell Lumen the URIs it should respond to
  | and give it the Closure to call when that URI is requested.
  |
 */

/**
 * Inkombizz | inkombizz@gmail.com | inkombizz.com
 * Route for Documentation.
 */
$router->get('/', function () {
    return "API";
});
$router->get('/api/v1/sync-host', function (Request $request) {
  return CronController::sync_host($request);
});
$router->get('/api/v1/sync-config-host', function (Request $request) {
  return CronController::sync_config_host($request);
});
$router->get('/api/v1/sync-ont', function (Request $request) {
  return CronController::sync_ont($request);
});

// /**
//  * Inkombizz | inkombizz@gmail.com | inkombizz.com
//  * Route Group for API.
//  */
// $router->group(['prefix' => 'api', 'namespace' => 'API'], function () use ($router) {
//     /**
//      * Inkombizz | inkombizz@gmail.com | inkombizz.com
//      * Route Group for Version 1.
//      */
//     $router->group(['prefix' => 'v1', 'namespace' => 'v1'], function () use ($router) {
//         /**
//          * Inkombizz | inkombizz@gmail.com | inkombizz.com
//          * Route for Customer.
//          */
//          $router->get('/sync-host', 'CronController@sync_host');
//          $router->get('/sync-config-host', 'CronController@sync_config_host');
//          $router->get('/sync-ont', 'CronController@sync_ont');
//     });
// });
